const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);

describe('GET /', function () {
    it('Response should be "Main page"', function (done) {
        chai.request(app)
            .get('/')
            .end((err, res) => {
                chai.expect(res.text).to.equal('Main page');
                done();
            })
    });

});
describe('GET /hello', function () {
    it('Response should be an object { result: "Hello, world!" }', function (done) {
        chai.request(app)
            .get('/hello')
            .end((err, res) => {
                chai.expect(res.body).to.be.an('object');
                chai.expect(res.body).to.eql({ result: 'Hello, world!' });
                done();
            })
    })
});